The UI linked to from this playbook doesn't show the section sign or anchors for section level 2 (==), and 3 (===). 

The UI repository is available on https://gitlab.com/norte73/ps-antora-ui.git.

To build a test site with the UI, follow the instructions below.

1.  Clone this playbook project repository to your computer.
2. See the [Antora quickstart](https://docs.antora.org/antora/latest/install-and-run-quickstart/) for instructions on how to set up Antora.
